<?php
namespace app\index\controller;

use think\Controller;
use think\Db;
use think\Request;
use PHPMailer\PHPMailer;

class SqlMailer extends Controller {
    // 从数据库中取出所有未发送的条目 并进行发送 [call]
    static public function Send() {
        $log_func = config("MAIL_LOG_FUNC"); // 日志函数
        if (!Request::instance()->isCli()) { // 只允许命令行调用
            $log_func("非法的请求");
            echo "非法的请求";
            exit;
        }

        $mail = new PHPMailer;

        $mail->CharSet = "utf-8";                       // 编码
        $mail->isSMTP();                                // 协议
        $mail->isHTML();                                // 内容格式
        $mail->SMTPAuth = true;                         // 需要认证
        $mail->SMTPDebug = 0;                           // 调试模式

        $mail->Host       = config("MAIL_SERVER_HOST"); // 服务器
        $mail->Port       = config("MAIL_SERVER_PORT"); // 端口
        $mail->SMTPSecure = config("MAIL_SMTP_SECURE"); // 加密方式可为空
        $mail->Username   = config("MAIL_USERNAME");    // 帐号
        $mail->Password   = config("MAIL_PASSWORD");    // 密码/授权码

        $mail_list = Db::name("mail")
            ->where("sent", false)
            ->select();
        /*
        id           int
        submit_time  time
        from_name    text
        to_addr_list json
        subject      text
        html_body    text
        sent         bool
        */

        foreach ($mail_list as $item) {
            $mail->setFrom($mail->Username, $item["from_name"]);
            $to_addr = json_decode($item["to_addr_list"]);
            foreach ($to_addr as $addr)
                $mail->AddAddress($addr);
            $mail->Subject = $item["subject"];
            $mail->Body = $item["html_body"];
            try {
                $mail->send();
                Db::name("mail")
                    ->where("id", $item["id"])
                    ->update(["sent" => true]); // 防止重复发送 发送一个标记一个
                $log_func("邮件发送成功，id: " . $item["id"] . " -> " . $item["to_addr_list"]);
            }
            catch (\Throwable $e) { 
                $log_func("模板消息发送错误: " . $e->getMessage(), "except");
            }
            $mail->clearAllRecipients();
        }
        echo "send done";
	}
}
